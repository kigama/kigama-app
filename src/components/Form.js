import React, { Component } from 'react';
import {task} from '../task.json'

class Formz extends Component{
  constructor(){
    super();
    this.state = {
      task: task
    }
  }
    render(){
        return(
        <nav className="navbar navbar-dark bg-dark">
          <a href="" className="text-white">
          { this.props.titulo}
          <span className="badge badge-pill badge-light ml-2">
          {this.state.task.length}
          </span>
          </a>
        </nav>
        )

    }
}
export default Form;